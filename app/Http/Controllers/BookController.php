<?php

namespace App\Http\Controllers;

use App\Book;
use App\Countrie;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('books.index',[
            'books' => Book::paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create',[
            'countries' => Countrie::get(),
        ]);
    }

    public function get_city(Request $request){
        $cities = City::where('country_id',$request->country)->get();
        return response()->json($cities);
    }
    /**
     * Get a validator for an incoming boook request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255|unique:books',
            'author_name' => 'required|string|max:255',
            'author_last_name' => 'required|string',
            'creation_year' => 'required',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $main_image = $request->file('file');
            $file_name = $main_image->getClientOriginalName();
            $destinationPath = public_path('/uploads/books');
            $main_image->move($destinationPath, $file_name);
        }else{
            $file_name = '';
        }
        $this->validator($request->all())->validate();

        Book::create([
            'title' => $request->title,
            'author_name' => $request->author_name,
            'city' => $request->city,
            'country' => $request->country,
            'author_last_name' => $request->author_last_name,
            'creation_year' => $request->creation_year,
            'isbn' => $request->isbn,
            'desc' => $request->desc,
            'file' => $file_name,
        ]);
        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('books.edit', [
            'countries' => Countrie::get(),
            'citis' => City::where('country_id', $book->country)->get(),
            'book'   => $book,
            'delimiter'  => ''
        ]);
    }

    /**
     * Get a validator for an incoming boook request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function editValidator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255',
            'author_name' => 'required|string|max:255',
            'author_last_name' => 'required|string',
            'creation_year' => 'required',
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->editValidator($request->all())->validate();
        if ($request->hasFile('file')) {
            $main_image = $request->file('file');
            $file_name = $main_image->getClientOriginalName();
            $destinationPath = public_path('/uploads/books');
            $main_image->move($destinationPath, $file_name);
        }else{
            $file_name = $book->file;
        }

        $book->update([
            'title' => $request->title,
            'author_name' => $request->author_name,
            'city' => $request->city,
            'country' => $request->country,
            'author_last_name' => $request->author_last_name,
            'creation_year' => $request->creation_year,
            'desc' => $request->desc,
            'isbn' => $request->isbn,
            'file' => $file_name,
        ]);

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        Book::where('id',$book->id)->update([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        return redirect()->route('books.index');
    }

}
