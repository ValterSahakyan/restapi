<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Carbon\Carbon;

class ApiController extends Controller
{
    public function index(){
        return view('api.index');
    }
    public function documentation(){
        return view('api.doc');
    }
    public function api(Request $request)
    {
        if ($request->api_token) {
            $user = User::where('api_token', '=', $request->api_token)->first();
            if ($user && $request->api_token === $user->api_token) {
                $limit = 10;
                $column = 'id';
                $direction = 'ASC';
                if ($request->limit) {
                    $limit = $request->limit;
                }
                if ($request->column) {
                    $column = $request->column;
                }
                if ($request->direction) {
                    $direction = $request->direction;
                }
                $query = Book::query();
                if ($request->q && $request->q != '') {
                    $query->where('title', 'like', '%' . $request->q . '%')
                        ->orWhere('author_name', 'like', '%' . $request->q . '%')
                        ->orWhere('author_last_name', 'like', '%' . $request->q . '%')
                        ->orWhere('isbn', 'like', '%' . $request->q . '%');
                }
                if ($request->id) {
                    $query->orWhere('id', $request->id);
                }
                if ($request->title) {
                    $query->orWhere('title', 'like', '%' . $request->title . '%');
                }
                if ($request->author_name) {
                    $query->orWhere('author_name', 'like', '%' . $request->author_name . '%');
                }
                if ($request->author_last_name) {
                    $query->orWhere('author_last_name', 'like', '%' . $request->author_last_name . '%');
                }
                if ($request->isbn) {
                    $query->orWhere('isbn', $request->isbn);
                }
                if ($request->creation_date && $request->creation_date != '' && $request->creation_date != null) {
                    $creation_date = $request->creation_date;
                    if ($creation_date == 'today') {
                        $from = Carbon::today();
                        $query->orWhereDate('creation_year', $from);
                    }
                    if ($creation_date == 'this_week') {
                        $from = Carbon::now()->startOfWeek();
                        $to = Carbon::now()->endOfWeek();
                        $query->orWhereBetween('creation_year', [$from, $to]);
                    }
                    if ($creation_date == 'this_month') {
                        $from = Carbon::now()->startOfMonth();
                        $to = Carbon::now()->endOfMonth();
                        $query->orWhereBetween('creation_year', [$from, $to]);
                    }
                    if ($creation_date == 'this_year') {
                        $from = Carbon::now()->startOfYear();
                        $to = Carbon::now()->endOfYear();
                        $query->orWhereBetween('creation_year', [$from, $to]);
                    }
                }
                $books = $query->take($limit)->orderBy($column, $direction)->get();
                if (count($books) > 0) {
                    return response()->json($books);
                } else {
                    return "<div class='alert alert-danger'>The URL you gave is not valid. Status code 404.</div>";
                }
            }else{
                return "<div class='alert alert-danger'>Unauthorized user:</div>";
            }
        }else{
            return "<div class='alert alert-danger'>Your Token not valid.</div>";
        }
    }
}
