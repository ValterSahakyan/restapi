<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Book extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'author_name', 'city', 'file', 'desc', 'country', 'author_last_name', 'creation_year', 'isbn'];

    protected $dates = ['deleted_at'];
//    protected $softDelete = true;
    public function setIsbnAttribute() {
        $pool = '0123456789';
        $this->attributes['isbn'] = substr(str_shuffle(str_repeat($pool, 9)), 0, 9);
    }

}
