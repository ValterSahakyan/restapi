@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-5">Book Rest Api</h2>
                    <form>
                        <input type="hidden" name="api_token" value="<?php if(auth()->user()){echo auth()->user()->api_token;}?>">
                        <div class="form-group row">
                            <label for="q" class="col-sm-2 col-form-label">q</label>
                            <div class="col-sm-10">
                                <input type="text" name="q" class="form-control"  placeholder="Search by all keywords" id="q">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 col-form-label">Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control" placeholder="Search by Title" id="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author_name" class="col-sm-2 col-form-label">Author Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="author_name" class="form-control" placeholder="Search by Author Name" id="author_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author_last_name" class="col-sm-2 col-form-label">Author Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="author_last_name" class="form-control" placeholder="Search by Author Last Name" id="author_last_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isbn" class="col-sm-2 col-form-label">ISBN</label>
                            <div class="col-sm-10">
                                <input type="text" name="isbn" class="form-control" placeholder="Search by ISBN" id="isbn">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isbn" class="col-sm-2 col-form-label">Creation Date</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="creation_date" id="creation_date">
                                    <option value="">Search by date</option>
                                    <option value="today">Today</option>
                                    <option value="this_week">This Week</option>
                                    <option value="this_month">This Month</option>
                                    <option value="this_year">This Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="button" id="getBook" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>

            </div>
        </div>
        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <div class="container" id="result">

            </div>
        </div>

    </div>

@endsection
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $("#getBook").click(function(){
            var q = $("input[name=q]").val();
            var title = $("input[name=title]").val();
            var author_name = $("input[name=author_name]").val();
            var author_last_name = $("input[name=author_last_name]").val();
            var isbn = $("input[name=isbn]").val();
            var creation_date = $("#creation_date").val();
            var api_token =$("input[name=api_token]").val()
            $.ajax({
                url: "{{route('api')}}",
                type: 'GET',
                data: {api_token:api_token,q:q,title:title,author_name:author_name,author_last_name:author_last_name,isbn:isbn,creation_date:creation_date},
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $( "#result" ).empty();
                    $.each( data, function( key, value ) {
                        $('#result').append('<div class="row">{ </br>' +
                            '"id": '+value.id+'</br>' +
                            '"title": '+value.title+'</br>' +
                            '"author_name": '+value.author_name+'</br>' +
                            '"author_last_name": '+value.author_last_name+'</br>' +
                            '"creation_year": '+value.creation_year+'</br>' +
                            '"isbn": '+value.isbn+'</br>' +
                            '"city": '+value.city+'</br>' +
                            '"country": '+value.country+'</br>' +
                            '"description": '+value.desc+'</br>' +
                            '"image": '+value.file+'</br>' +
                            ' }</div>')
                    });
                },
                error: function (data) {
                    $('#result').append('<div class="row">'+data.responseText+'</div>')
                },
            });
        });
    })
</script>
