@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <h2 class="text-center mb-5">Documentation Rest Api</h2>
                    <div class="col-md-12">
                        <h3> Each endpoint is individually documented below; be aware that endpoints share common formats for returned data.</h3>
                        <hr/>
                        <h3>You will get the  <b>api token</b> via mail when authentication is passed successfully.</h3>
                        <hr/>
                        <h3> Test Token - <strong><code class="highlighter-rouge">icZifa4imW9cJqovfq1y</code></strong>.</h3>
                    </div>
                    <hr/>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y"> {{url('/')}}/api?api_token={your token}</a>
                        </p>
                        <span>Returns first 10 books by authorized user token.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&q=Predovic-Kassulke"> {{url('/')}}/api?api_token={your token}&q={any string as a keyword}</a>
                        </p>
                        <span>Returns books by any string as a keyword.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&id=8"> {{url('/')}}/api?api_token={your token}&id={id}</a>
                        </p>
                        <span>Returns books by book ID.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&title=Bergnaum-Denesik"> {{url('/')}}/api?api_token={your token}&title={title}</a>
                        </p>
                        <span>Returns books by book title.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&author_name=Camilla"> {{url('/')}}/api?api_token={your token}&author_name={author_name}</a>
                        </p>
                        <span>Returns books by book Author Name.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&author_last_name=Stamm"> {{url('/')}}/api?api_token={your token}&author_last_name={author_last_name}</a>
                        </p>
                        <span>Returns books by book Author Last Aame.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&isbn=304948244"> {{url('/')}}/api?api_token={your token}&isbn={isbn}</a>
                        </p>
                        <span>Returns books by book ISBN.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&creation_date=this_year"> {{url('/')}}/api?api_token={your token}&creation_date={this_week}</a>
                        </p>
                        <span>Returns books by book Creation Date.</span>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        <p>
                            <a target="_blank" href="{{url('/')}}/api/?api_token=icZifa4imW9cJqovfq1y&id=8&title=Cole-Durgan&author_name=Zoila&author_last_name=Klein&isbn=390393505&limit=1&column=id&direction=ASC&creation_date="> {{url('/')}}/api?api_token={your token}&id={id}&title={title}&author_name={author_name}&author_last_name={author_last_name}&isbn={isbn}&limit={limit(int)}&column={column}&direction={(ASC,DESC)}&creation_date={this_week}</a>
                        </p>
                        <span>Returns books by book all fields.</span>
                    </div>
                    <hr>
                <table class="table table-striped table-bordered">
                    <thead>
                    <th>Name</th>
                    <th>Type</th>
                    <th>REQUIRED</th>
                    <th>DESCRIPTION</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>api_token</td>
                            <td>string</td>
                            <td>Yes</td>
                            <td>Personal Token.</td>
                        </tr>
                        <tr>
                            <td>q</td>
                            <td>string</td>
                            <td>No</td>
                            <td>Return books matching the given keywords. This parameter will accept any string as a keyword.</td>
                        </tr>
                        <tr>
                            <td>id</td>
                            <td>integer</td>
                            <td>No</td>
                            <td>Return books by ID.</td>
                        </tr>
                        <tr>
                            <td>title</td>
                            <td>string</td>
                            <td>No</td>
                            <td>Return books by title.</td>
                        </tr>
                        <tr>
                            <td>author_name</td>
                            <td>string</td>
                            <td>No</td>
                            <td>Return books by Author Name.</td>
                        </tr>
                        <tr>
                            <td>author_last_name</td>
                            <td>string</td>
                            <td>No</td>
                            <td>Return books by Author Last Name.</td>
                        </tr>
                        <tr>
                            <td>isbn</td>
                            <td>integer</td>
                            <td>No</td>
                            <td>Return books by ISBN.</td>
                        </tr>
                        <tr>
                            <td>limit</td>
                            <td>integer</td>
                            <td>No</td>
                            <td>Return books Limit.</td>
                        </tr>
                        <tr>
                            <td>orderBy</td>
                            <td>string||integer</td>
                            <td>No</td>
                            <td>Parameter you want to sort by - column (id,title,author_name,author_last_name,isbn),direction(ASC,DESC).</td>
                        </tr>
                        <tr>
                            <td>creation_date.keyword</td>
                            <td>string</td>
                            <td>No</td>
                            <td>Only return books with creation dates within the given keyword date range. Keyword options are “today”, “this_week”, “this_month”, “this_year” </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
