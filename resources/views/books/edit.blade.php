@extends('layouts.app')

@section('content')
    <div class="container">
        <hr />
        <form class="form-horizontal book_form" enctype="multipart/form-data" action="{{route('books.update',$book)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="title" class="col-sm-2 col-form-label">Title</label>
                <div class="col-md-10">
                    <input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="title" value="{{ $book->title }}">
                    @if ($errors->has('title'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="isbn" class="col-sm-2 col-form-label">ISBN (Unique value)</label>
                <div class="col-md-10">
                    <input  type="text" class="form-control" placeholder="Automatic generation" id="isbn" name="isbn" value="{{$book->isbn}}" readonly="">
                </div>
            </div>
            <hr />
            <div class="form-group row">
                <label for="desc" class="col-sm-2 col-form-label">Description</label>
                <div class="col-md-10">
                    <textarea class="form-control" id="desc" name="desc" rows="3">{{$book->desc}}</textarea>
                </div>
            </div>
            <hr />
            <div class="form-group row">
                <label for="datepicker" class="col-sm-2 col-form-label">Creation Year</label>
                <div class="col-md-10">
                    <input type="text" class="form-control {{ $errors->has('creation_year') ? ' is-invalid' : '' }}" value="{{ $book->creation_year }}" name="creation_year" id="datepicker">
                    @if ($errors->has('creation_year'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('creation_year') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="author_name" class="col-sm-2 col-form-label">Author Name</label>
                <div class="col-md-10">
                    <input type="text" class="form-control {{ $errors->has('author_name') ? ' is-invalid' : '' }}" value="{{ $book->author_name }}" id="author_name" name="author_name">
                    @if ($errors->has('author_name'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('author_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="author_last_name" class="col-sm-2 col-form-label">Author Last Name</label>
                <div class="col-md-10">
                    <input type="text" class="form-control {{ $errors->has('author_last_name') ? ' is-invalid' : '' }}" value="{{ $book->author_last_name }}" id="author_last_name" name="author_last_name">
                    @if ($errors->has('author_last_name'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('author_last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="isbn" class="col-sm-2 col-form-label">Countries</label>
                <div class="col-sm-10">
                    <select class="form-control" data-live-search="true" name="country" id="countries">
                        <option value="">Select Countries</option>
                        @foreach ($countries as $countrie)
                            <option
                                    @isset($book->country)
                                            @if ($countrie->id == $book->country)
                                                selected="selected"
                                            @endif
                                    @endisset
                                    value="{{$countrie->id}}">{{$countrie->country_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="isbn" class="col-sm-2 col-form-label">Cities</label>
                <div class="col-sm-10">
                    <select class="form-control" data-live-search="true" name="city" id="cities">
                        <option value="">Select Cities</option>
                        @foreach ($citis as $city)
                            <option
                                    @isset($book->city)
                                    @if ($city->id == $book->city)
                                    selected="selected"
                                    @endif
                                    @endisset
                                    value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="file" class="col-sm-2 col-form-label">Image</label>
                <div class="col-md-10 custom-file">
                    <input type="file" name="file" class="custom-file-input" id="file">
                    <label class="custom-file-label" for="file">{{$book->file or 'Choose file...'}}</label>
                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                </div>
            </div>
            <hr/>
            <div class="form-group row mb-0">
                <div class="col-md-12 text-center">
                    <input class="btn btn-primary" type="submit" value="Update">
                </div>
            </div>
        </form>
    </div>
    <script>
        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd hh:mm:ss',
            uiLibrary: 'bootstrap4'
        });
        $('#countries').on('change', function() {
            var country = this.value;
            $.ajax({
                url: "{{route('get_city')}}",
                type: 'post',
                data: {country:country},
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#cities')
                        .find('option')
                        .remove()
                        .end();
                    $.each( data, function( key, value ) {
                        $('#cities').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                },
                error: function (data) {

                },
            });
        });
    </script>
@endsection