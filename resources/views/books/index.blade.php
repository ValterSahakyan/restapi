@extends('layouts.app')

@section('content')

    <div class="container">
        <hr>
        <a href="{{route('books.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Create Book
        </a>
        <table class="table table-striped">
            <thead>
            <th>Title</th>
            <th>Author Name</th>
            <th>Author Last Name</th>
            <th>Year of creation</th>
            <th>ISBN</th>
            <th>Country</th>
            <th>City</th>
            <th class="text-right">Action</th>
            </thead>
            <tbody>
            @forelse ($books as $book)
                <?php
                    $country = App\Countrie::where('id', $book->country)->first();
                    $country = json_decode($country, true);
                    $city = App\City::where('id',$book->city)->first();
                    $city = json_decode($city, true);
                ?>
                <tr>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$book->title}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$book->author_name}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$book->author_last_name}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{date('d-m-Y', strtotime($book->creation_year))}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$book->isbn}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$country['country_name']}}</td>
                    <td data-toggle="modal" data-target="#bookModal{{$book->id}}" class="show book_{{$book->id}}" data-id="{{$book->id}}">{{$city['name']}}</td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Delete ?')){return true}else{return false}" action="{{route('books.destroy',$book)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field()}}
                            <a class="btn btn-default" href="{{route('books.edit', $book)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
                <div class="modal fade" id="bookModal{{$book->id}}" tabindex="-1" role="dialog" aria-labelledby="bookModal" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">{{$book->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="col-md-12 lib-item" data-category="view">
                                <div class="lib-panel">
                                    <div class="row box-shadow">
                                        <div class="col-md-6">
                                            <img class="lib-img-show" src="/uploads/books/{{$book->file}}">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="lib-row lib-header">
                                                {{$book->author_name}}  {{$book->author_last_name}}
                                                <div class="lib-header-seperator"></div>
                                            </div>
                                            <div class="lib-row lib-desc">
                                                <h4>{{$book->isbn}}</h4>
                                                {{ str_limit($book->desc, 120) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row box-shadow">
                                        <div class="col-md-12">
                                            <hr/>
                                            <span>{{date('d-m-Y', strtotime($book->creation_year))}}</span>
                                            ||
                                            <span>{{$country['country_name']}} {{$city['name']}}</span>
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <tr>
                    <td colspan="12" class="text-center"><h2>No data available</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                        {{ $books->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
