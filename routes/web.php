<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/doc', 'ApiController@documentation')->name('doc');
Route::get('/get-books', 'GetBookController@index')->name('get-books');
Route::post('/get_city', 'BookController@get_city')->name('get_city');
Route::get('/api', 'ApiController@api')->name('api');
Route::post('/api', 'ApiController@api')->name('api');
Route::get('/restapi', 'ApiController@index')->name('restapi');
Route::resource('/books', 'BookController');


Route::get('/', 'BookController@index');
Auth::routes();

Route::get('/books', 'BookController@index')->name('books.index');
Route::get('/home', 'BookController@index');
